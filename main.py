import requests
import json
import time
import picamera
import os
from datetime import datetime

########################## PARAMETERS ##############################

local_dir = "/home/raspberrycam/" # local folder for records
video_length = 240 # video length in second
gelements_event = "homecoming" # specify an event to monitor >> intrusion, button, camera, door, homecoming, camera_recording, hue, motion, phone, plug, siren, smoke, systemhealth, water, window
gelements_email = "adresse@email.com" # Gigaset Elements account email
gelements_passwd = "YourPassword" # Gigaset Elements account passwod
ftp_srv = "FtpServerAddress" # FTP remote server
ftp_user = "FtpUser" # FTP remote username
ftp_passwd = "FtpPassword" # FTP remote password
curl_options = "-k --ftp-ssl" # cURL options to send file to remote FTP server

###################### DON'T MODIFY BELOW #########################

def record_vid():
    now = datetime.now()
    date_time = now.strftime("%d-%m-%-y_%Hh%Mm%Ss")

    try:
        camera = picamera.PiCamera()
    except:
        print(f'{date_time} - ERROR : Unable to initialize Raspberry Camera')

    try:
        camera.start_recording(f'{local_dir}{date_time}.h264')
        camera.wait_recording(video_length)
        camera.stop_recording()
    except:
        print(f'{date_time} - ERROR : Unable to start video recording')
    try:
        os.system(f'ffmpeg -hide_banner -loglevel error -framerate 24 -i {local_dir}{date_time}.h264 -c copy {local_dir}{date_time}.mp4')
        os.system(f'rm -f {local_dir}{date_time}.h264')
        
    except:
        print(f'{date_time} - ERROR : Unable to convert video in MP4')
    try:
        os.system(f'curl -sS -T {local_dir}{date_time}.mp4 ftp://{ftp_srv} --user {ftp_user}:{ftp_passwd} {curl_options}')
    except:
        print(f'{date_time} - ERROR : Unable to upload video to remote FTP server')

    camera.close()

s = requests.Session()
cred = {}
cred['email'] = gelements_email
cred['password'] = gelements_passwd
cred['from'] = "elements_wfe2"
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'en-US,en;q=0.5',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Origin': 'https://app.gigaset-elements.com',
    'Connection': 'keep-alive',
    'Referer': 'https://app.gigaset-elements.com/',
}
url_auth = 'https://im.gigaset-elements.com/identity/api/v1/user/login'
url_iden = 'https://api.gigaset-elements.com/api/v1/auth/openid/begin?op=gigaset_elements&return_to=https://app.gigaset-elements.com/#/'
url_json = f'https://api.gigaset-elements.com/api/v2/me/events?limit=10&group={gelements_event}'
temoina = True

while temoina:
    know = datetime.now()
    kdate_time = know.strftime("%d-%m-%Y %H:%M:%S.%f")
    temoinb = 0
    temoinc = 0

    print(f'{kdate_time} - Starting session')

    try:
        s.post(url_auth, headers=headers, data=cred)
        s.get(url_iden)

        jso = s.get(url_json)
        jso = jso.json()

        last_ts = jso['events'][0]['ts']
    except:
        print(f'{kdate_time} - ERROR : Unable to join Gigaset Elements API')
        s.close()
        time.sleep(60)
        temoina += 1
        continue

    while temoinb <= 2880 and temoinc <= 10:
        know = datetime.now()
        kdate_time = know.strftime("%d-%m-%Y %H:%M:%S.%f")
        try:
            jso = s.get(url_json)
            jso = jso.json()
            if jso['events'][0]['ts'] != last_ts:
                print(f'{kdate_time} - INFO : New event, starting video record')
                record_vid()
                print(f'{kdate_time} - INFO : New event, video record ended')
                last_ts = jso['events'][0]['ts']
                continue
        except:
            print(f'{kdate_time} - ERROR : Unable to download events historic from Gigaset Elements API')
            temoinc += 1
        time.sleep(5)
        temoinb += 1

    try:
        s.close()
        print(f'{kdate_time} - Closing session')
        time.sleep(60)
    except:
        print(f'{kdate_time} - ERROR : Unable to close the session')
