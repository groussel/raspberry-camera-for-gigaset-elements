# Raspberry Camera for Gigaset Elements

This Python 3 script allows you to connect a Raspberry Camera to your Gigaset Elements Smart Home installation.
Works in Linux / MacOS systems.

## Features 

- Record video from Raspberry Camera where events detected on Gigaset Elements system
- Convert h.264 output video to MP4
- Send video to FTP remote server
- Highly customizable

## Requirements

- Raspberry Camera enabled
- cURL
- FFmpeg
- Python requests module
- Python json module
- Python picamera module

## Usage

- Edit main.py
- Modify the PARAMETERS section with your informations
- Execute the script
